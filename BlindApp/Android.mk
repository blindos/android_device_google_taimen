LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := BlindApp
LOCAL_CERTIFICATE := platform

ifeq ($(PLATFORM_SDK_VERSION), 25)
  LOCAL_SRC_FILES := BlindApp.apk
else
  LOCAL_SRC_FILES := BlindApp.apk
endif

LOCAL_MODULE_CLASS := APPS
LOCAL_PRIVILEGED_MODULE := true
LOCAL_MODULE_PATH := $(TARGET_OUT_APPS)
LOCAL_OVERRIDES_PACKAGES := Home  PixelLauncher Launcher2 Launcher3 Launcher3Go Launcher3QuickStep Launcher3QuickStepGo Fluctuation Trebuchet TrebuchetQuickStep


LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
include $(BUILD_PREBUILT)
